object FAbout: TFAbout
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsNone
  Caption = 'FAbout'
  ClientHeight = 321
  ClientWidth = 713
  Color = 3946803
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Signika'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  ScreenSnap = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 22
  object Image1: TImage
    Left = 8
    Top = 8
    Width = 697
    Height = 305
  end
  object titL: TLabel
    Left = 40
    Top = 32
    Width = 221
    Height = 39
    Caption = 'Orar Facultate 2'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -31
    Font.Name = 'Signika'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 40
    Top = 69
    Width = 143
    Height = 31
    Caption = 'by BogdyBBA'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14999772
    Font.Height = -25
    Font.Name = 'Signika'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 56
    Top = 128
    Width = 137
    Height = 22
    Caption = 'version 2.0 alpha'
  end
  object Label3: TLabel
    Left = 206
    Top = 128
    Width = 468
    Height = 22
    Caption = 'January 28th, February 2nd, 4th, 5th, 7th and 18th of 2013'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -19
    Font.Name = 'Signika'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 56
    Top = 156
    Width = 127
    Height = 22
    Caption = 'version 2.0 beta'
  end
  object Label5: TLabel
    Left = 206
    Top = 156
    Width = 341
    Height = 22
    Caption = 'February 23rd and 26th, March 1st of 2013'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -19
    Font.Name = 'Signika'
    Font.Style = []
    ParentFont = False
  end
  object OkB: TImage
    Left = 266
    Top = 208
    Width = 200
    Height = 75
    Cursor = crHandPoint
    Hint = 'Ok'
    OnClick = OkBClick
  end
end
