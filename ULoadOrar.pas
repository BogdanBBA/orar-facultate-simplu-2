unit ULoadOrar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFLoadOrar = class(TForm)
    Image1: TImage;
    CancelB: TImage;
    lb: TListBox;
    titL: TLabel;
    subtitL: TLabel;
    LoadB: TImage;
    refL: TLabel;
    testL: TLabel;
    Button1: TButton;
    testT: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure CancelBClick(Sender: TObject);
    procedure refLMouseEnter(Sender: TObject);
    procedure refLMouseLeave(Sender: TObject);
    procedure refLClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbClick(Sender: TObject);
    procedure testTTimer(Sender: TObject);
    procedure LoadBClick(Sender: TObject);
    procedure lbDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLoadOrar: TFLoadOrar;
  sx: string;
  errL: TStringList;
  doubleClicked: boolean;

implementation

{$R *.dfm}

uses Main, DataTypes, VCLCustomMethods;

procedure TFLoadOrar.CancelBClick(Sender: TObject);
begin
  if failedToLoadLastUsedOrar then
    begin
      MessageDlg('The default orar could not be loaded and you have chosen to cancel loading another one. The program will now close.', mtWarning, [mbOK], 0);
      Halt
    end;
  FLoadOrar.Close
end;

procedure TFLoadOrar.FormCreate(Sender: TObject);
begin
  ColorImagePanel(Image1, colForm, colButtonMouseOver);
  CancelB.OnMouseEnter:=FMain.ButtonIMGMouseEnter;
  CancelB.OnMouseLeave:=FMain.ButtonIMGMouseLeave; LoadB.OnMouseLeave:=FMain.ButtonIMGMouseLeave;
  CancelB.OnMouseLeave(CancelB); LoadB.OnMouseLeave(LoadB)
end;

procedure TFLoadOrar.refLMouseEnter(Sender: TObject);
begin refL.Font.Color:=titL.Font.Color; refL.Font.Style:=[fsBold] end;

procedure TFLoadOrar.refLMouseLeave(Sender: TObject);
begin refL.Font.Color:=subtitL.Font.Color; refL.Font.Style:=[] end;

//

procedure TFLoadOrar.FormShow(Sender: TObject);
begin
  refLClick(FloadOrar)
end;

procedure TFLoadOrar.refLClick(Sender: TObject);
var list: TStringList;
begin
  refLMouseLeave(refL);
  list:=TStringList.Create; list.Sorted:=true; list.Duplicates:=dupIgnore;
  ScanForFiles(GetCurrentDir+'\data\', 'xml', list);
  if list.IndexOf('settings.xml')<>-1 then list.Delete(list.IndexOf('settings.xml'));
  lb.Clear; lb.Items:=list;
  LoadB.OnMouseEnter:=nil; LoadB.Cursor:=crDefault;
  doubleClicked:=false; Button1.SetFocus
end;

procedure TFLoadOrar.lbClick(Sender: TObject);
begin
  if lb.ItemIndex=-1 then exit;
  sx:='data\'+lb.Items[lb.ItemIndex];
  testL.Caption:=Format('�ncerc sa �ncarc "%s"...', [sx]);
  testL.Show; testT.Enabled:=true
end;

procedure TFLoadOrar.lbDblClick(Sender: TObject);
begin doubleClicked:=true end;

procedure TFLoadOrar.testTTimer(Sender: TObject);
begin
  testT.Enabled:=false;
  if not LoadOrar(sx, testOrar, errL) then
    begin
      ShowErrorList(mtWarning, 'Sorry, the file "'+sx+'"could not be correctly loaded. See why:', 'I don''t know what to say.', errL);
      LoadB.OnMouseEnter:=nil; LoadB.Cursor:=crDefault
    end
  else
    begin
      LoadB.OnMouseEnter:=FMain.ButtonIMGMouseEnter; LoadB.Cursor:=crHandPoint;
      if doubleClicked then LoadBClick(testT)
    end;
  testL.Hide; LoadB.OnMouseLeave(LoadB); doubleClicked:=false
end;

procedure TFLoadOrar.LoadBClick(Sender: TObject);
begin
  if (LoadB.Cursor=crDefault) or testT.Enabled then exit;
  LoadOrar(sx, Orar, errL);

  Sett.lastUsedProject:=copy(ExtractFileName(sx), 1, length(ExtractFileName(sx))-4);
  if not SaveSettings then MessageDlg('The settings could not be saved.', mtWarning, [mbIgnore], 0);
  failedToLoadLastUsedOrar:=false;

  CancelBClick(LoadB);
  FMain.RefreshOrar1Click(LoadB);
  FMain.Ceamaiapropriatzidecurs1Click(LoadB)
end;

end.
