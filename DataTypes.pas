﻿unit DataTypes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellAPI, Menus, ExtCtrls, jpeg, PngImage, ImgList,
  ComCtrls, MPlayer, ToolWin, XML.VerySimple, UIntList, DateUtils;

const
  nl=#13#10; dnl=nl+nl; KB=1024; MB=KB*KB;

  NFo: array[0..1] of string=('data', 'files');
  NFi: array[0..0] of string=('data\settings.xml');
  ZiSapt: array[1..7] of string=('Luni', 'Marți', 'Miercuri', 'Joi', 'Vineri', 'Sâmbătă', 'Duminică');
  ZiSaptS: array[1..7] of string=('Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica');
  LunaAnului: array[1..12] of string=('Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie');

  ioBoxHeight_AUTO_SEM = 1;
  ioBoxHeight_AUTO_SEL = 2;

type TSaptamana=record
  incepandLuni: TDate;
  NumeSeturi: TStringList;
end;

type TIntervOrar=record
  deLa, panaLa: TDateTime;
  Disciplina, Tip, Sala, Profesor: string;
  Culoare: TColor;
end;

type TOrar=record
  XML: TXMLVerySimple;
  Info, Sapt, Orar: TXMLNode;
  nS, nIO, maxIOpeZi: word;
  Saptamani: array of TSaptamana;
  IntervOrar: array of TIntervOrar;
end;

type TSettings=record
  lastUsedProject: string;
  dayCount: byte;
  ioBoxHeight: byte;
end;

var
  Orar, testOrar: TOrar;
  Sett: TSettings;

  centerDate: TDate;

  CompletelyInitialized, CurrentlyClosing: boolean;

  f: textfile;

procedure log(s: string);
procedure ShowErrorList(msgType: TMsgDlgType; preMsg, postMsg: string; list: TStringList);
procedure ScanForFiles(directory, extension: string; var list: TStringList);
//
function GetDateForOrarTitleI(i: integer): TDate; overload;
function GetDateForOrarTitleI(img: TImage): TDate; overload;
function FirstIOPoz(co: TOrar; aDate: TDate): integer;
function nIOforDate(co: TOrar; aDate: TDate): byte;
function nMinutes(t1, t2: TTime): longint;
procedure GetCurrentTimeRange(co: TOrar; ioHsett: byte; var t1, t2: TTime);
procedure GetIntervalPercentage(smallSinceT, smallToT, largeSinceT, largeToT: TTime; var Top, Height: real);
//
function LoadOrar(fileName: string; var co: TOrar; var errL: TStringList): boolean;
function LoadSettings(var errL: TStringList): boolean;
function SaveSettings: boolean;
function InitializationOK(var errL: TStringList): boolean;

implementation

uses Main, FunctiiBaza;

//ShellExecute(Handle, 'open', PChar(file_or_web_path), nil, nil, SW_SHOW)

procedure log(s: string);
var logf: textfile;
begin
  try assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    ', now), s); closefile(logf)
  except begin try assignfile(logf, 'data\log.txt'); append(logf); writeln(logf, formatdatetime('dd.mmm.yyyy hh:nn:ss    |    "Log retry: "', now), s); closefile(logf)
    except on E:Exception do MessageDlg('Failed a second time to log message "'+s+'"'+dnl+e.classname+' :  '+e.Message, mtError, [mbOK], 0) end end end
end;

procedure ShowErrorList(msgType: TMsgDlgType; preMsg, postMsg: string; list: TStringList);
var i: word; s: string;
begin
  if list.Count>0 then begin s:=preMsg+nl; for i:=0 to list.Count-1 do s:=s+nl+'- '+list[i]+';'; s:=s+dnl+postMsg end
  else s:=preMsg+dnl+'(There are no error messages in the list)'+dnl+postMsg;
  MessageDlg(s, msgType, [mbOK], 0)
end;

procedure ScanForFiles(directory, extension: string; var list: TStringList);
var search: TSearchRec;
begin
  if list=nil then begin showmessage('List is nil! Exiting ScanForFiles()...'); exit end;
  log('Scanning for files...'); // Subdirectories
  if FindFirst(directory + '*.'+ansiuppercase(extension), faDirectory, search) = 0 then
  begin
    repeat
      if (search.Name[1] <> '.') then list.Add(search.Name)
    until FindNext(search) <> 0;
    FindClose(search)
  end;
  log('Scan in "'+directory+'" successfull. NFiles = '+inttostr(list.Count))
end;

//

function GetDateForOrarTitleI(i: integer): TDate; overload;
var middle: byte;
begin
  try
    if i=-1 then begin result:=encodedate(1900, 1, 1); exit end;
    middle:=Sett.dayCount div 2+1;
    result := incDay(centerDate, i-middle)
  except on E:Exception do MessageDlg(Format('GetDateForOrarTitleI(integer) ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

function GetDateForOrarTitleI(img: TImage): TDate; overload;
var r: integer; i: byte; sx: string;
begin
  try
    sx:=copy(img.Hint, 1, 10);
    if DecodeMyKindOfDate(sx)<>encodedate(1900, 1, 1) then
      result := DecodeMyKindOfDate(sx)
    else
      begin
        r:=-1;
        for i:=1 to 7 do if dg[i].titleI<>nil then if dg[i].titleI=img then begin r:=i; break end;
        result := GetDateForOrarTitleI(r)
      end
  except on E:Exception do MessageDlg(Format('GetDateForOrarTitleI(img) ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

function FirstIOPoz(co: TOrar; aDate: TDate): integer;
var i: word;
begin
  try
  result:=-1;
  for i:=0 to co.nIO-1 do
    begin
      //showmessagefmt('%s / %s', [formatdatetime('yyyy.mm.dd', co.IntervOrar[i].deLa), formatdatetime('yyyy.mm.dd', aDate)]);
      if trunc(co.IntervOrar[i].deLa)=trunc(aDate) then
        begin result:=i; break end
    end //showmessagefmt('firstIO for aDate=%s, result=%d', [formatdatetime('d mmmm yyyy', aDate), result])
  except on E:Exception do MessageDlg(Format('FirstIOPoz ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

function nIOforDate(co: TOrar; aDate: TDate): byte;
var p: integer; n: word;
begin
  try
  p:=FirstIOPoz(co, aDate); //showmessagefmt('nIOforDate(d=%s): p=%d', [formatdatetime('yyyy.mm.dd', aDate), p]);
  if p=-1 then n:=0
  else
    begin
      n:=1;
      while (p<=co.nIO) and (trunc(co.IntervOrar[p+1].deLa)=trunc(aDate)) do begin inc(n); inc(p) end
    end;
  result := n
  except on E:Exception do MessageDlg(Format('nIOforDate ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

function nMinutes(t1, t2: TTime): longint;
var xt: TTime; nMin: longint;
begin
  try
    if t1>t2 then begin xt:=t1; t1:=t2; t2:=xt end;
    nMin:=0; xt:=t1; while frac(xt)<frac(t2) do begin inc(nMin); xt:=incMinute(xt) end;
    result:=nMin
  except on E:Exception do MessageDlg(Format('nMinute ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

procedure GetCurrentTimeRange(co: TOrar; ioHsett: byte; var t1, t2: TTime); //currently only works for orar on FMain and valid data
var i, p1, p2: integer; d: TDate;
begin
  try
  t1:=encodedatetime(1900, 1, 1, 0, 0, 0, 0); t2:=encodedatetime(1900, 1, 1, 0, 0, 0, 0);
  case ioHsett of
  1: begin p1:=0; p2:=co.nIO-1 end;
  2: begin
       i:=1; p1:=FirstIOPoz(co, GetDateForOrarTitleI(dg[i].titleI));
       while (p1=-1) and (i<5) do begin inc(i); p1:=FirstIOPoz(co, GetDateForOrarTitleI(dg[i].titleI)) end;
       i:=Sett.dayCount; d:=GetDateForOrarTitleI(dg[i].titleI); p2:=max(-1, FirstIOPoz(co, d)+nIOforDate(co, d)-1);
       while (p2=-1) and (i>1) do begin inc(i, -1); d:=GetDateForOrarTitleI(dg[i].titleI); p2:=max(-1, FirstIOPoz(co, d)+nIOforDate(co, d)-1) end
     end;
  else begin MessageDlg('GetCurrentTimeRange() ERROR: invalid Sett.ioBoxHeight='+inttostr(Sett.ioBoxHeight), mtError, [mbOK], 0); exit end
  end;                        //showmessagefmt('hey; ioHsett=%d, p1>=p2, p1=%d, p2=%d', [ioHsett, p1, p2]);
  if p1>=p2 then
    begin
      //MessageDlg(format('GetCurrentTimeRange() ERROR: p1>=p2, p1=%d, p2=%d', [p1, p2]), mtError, [mbOK], 0);
      Exit
    end;
  t1:=co.IntervOrar[p1].deLa; t2:=co.IntervOrar[p1].panaLa;
  //showmessagefmt('GetCurrentTimeRange: initial t1=%s, t2=%s', [formatdatetime('h:nn', t1), formatdatetime('h:nn', t2)]);
  for i:=p1+1 to p2 do
    begin
      //showmessagefmt('GetCurrentTimeRange: before for(i=%d(%d-%d)), t1=%s, t2=%s', [i, p1+1, p2, formatdatetime('h:nn', t1), formatdatetime('h:nn', t2)]);
      if frac(co.IntervOrar[i].deLa)   < frac(t1) then t1:=frac(co.IntervOrar[i].deLa);
      if frac(co.IntervOrar[i].panaLa) > frac(t2) then t2:=frac(co.IntervOrar[i].panaLa);
      //showmessagefmt('GetCurrentTimeRange: after for(i=%d(%d-%d)), t1=%s, t2=%s', [i, p1+1, p2, formatdatetime('h:nn', t1), formatdatetime('h:nn', t2)]);
    end;
  //showmessagefmt('GetCurrentTimeRange: final t1=%s, t2=%s', [formatdatetime('d/mmm/yyyy h:nn', t1), formatdatetime('d/mmm/yyyy h:nn', t2)]);
  except on E:Exception do MessageDlg(Format('GetCurrentTimeRange ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

procedure GetIntervalPercentage(smallSinceT, smallToT, largeSinceT, largeToT: TTime; var Top, Height: real);
var smallMin, diffMin, largeMin: longint;
begin
  try
  largeMin:=nMinutes(largeSinceT, largeToT); smallMin:=nMinutes(smallSinceT, smallToT); diffMin:=nMinutes(largeSinceT, smallSinceT);
    //showmessagefmt('small: %s - %s = %d minutes%slarge: %s - %s = %d minutes', [formatdatetime('h:nn', smallSinceT), formatdatetime('h:nn', smallToT), smallMin, dnl, formatdatetime('h:nn', largeSinceT), formatdatetime('h:nn', largeToT), largeMin]);
  if largeMin>0 then begin Top:=diffMin/largeMin*100; Height:=smallMin/largeMin*100 end
  else begin Top:=-1; Height:=50 end
    //showmessagefmt('top=%f%%, height=%f%%', [Top, Height])
  except on E:Exception do MessageDlg(Format('GetIntervalPercentage ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

//

function LoadOrar(fileName: string; var co: TOrar; var errL: TStringList): boolean; // Should first be loaded in temporary TOrar object
  {*}function AddIntervalOrar(var co: TOrar; disc, col: string; rest: TXMLNode): boolean;
  var xtip, xsala, xprof: string;
      cand, cnd, xs, xss: string;
      ws: TStringList; //ws=week sets
      i, j, k: word;
      xzi: byte;
      t1, t2: TTime;
      rdt: TDateTime;
      ok: boolean;
  begin
    try
      result:=false;
      ws:=TStringList.Create; ws.Sorted:=false; ws.Duplicates:=dupIgnore;
      xtip:=rest.Attribute['tip']; xsala:=rest.Attribute['unde']; xprof:=rest.Attribute['cu']; cand:=rest.Attribute['cand'];
      StrCleanup(cand, true, true);
      while cand<>'' do
        begin
          cnd:=copy(cand, 1, pos(')', cand)); delete(cand, 1, length(cnd)); //showmessagefmt('cnd="%s", cand="%s"', [cnd, cand]);
          xs:=copy(cnd, pos('(', cnd)+1, pos(';', cnd)-pos('(', cnd)-1); if xs[length(xs)]<>'+' then xs:=xs+'+'; ws.Clear; //showmessagefmt('xs="%s"', [xs]);
          while xs<>'' do
            begin
              xss:=copy(xs, 1, pos('+', xs)-1);
              if co.Sapt.Find('set', 'nume', xss)<>nil then ws.Add(xss); delete(xs, 1, pos('+', xs))
            end;
          if ws.Count=0 then exit; //showmessagefmt('ws.count=%d', [ws.Count])
          xzi := strtoint(copy(cnd, pos(';', cnd)+1, 1)); if not (xzi in [1..7]) then exit;
          t1 := DecodeMyKindOfTime(copy(cnd, pos('@', cnd)+1, pos('-', cnd)-pos('@', cnd)-1));
          t2 := DecodeMyKindOfTime(copy(cnd, pos('-', cnd)+1, pos(')', cnd)-pos('-', cnd)-1));
          if (formatdatetime('hh:nn:ss', t1)='00:00:01') or (formatdatetime('hh:nn:ss', t2)='00:00:01') then exit;
          //showmessagefmt('disciplina=%s, zi=%d, t1=%s, t2=%s', [disc, xzi, formatdatetime('hh:nn:ss', t1), formatdatetime('hh:nn:ss', t2)]);
          for i:=0 to co.nS-1 do
            begin
              ok:=false;
                for j:=0 to co.Saptamani[i].NumeSeturi.Count-1 do for k:=0 to ws.Count-1 do
                  if co.Saptamani[i].NumeSeturi[j]=ws[k] then ok:=true;
              if ok then //current cnd interval is ok for this week
                begin
                  rdt := incDay(co.Saptamani[i].incepandLuni, xzi-1);
                  inc(co.nIO); setlength(co.IntervOrar, co.nIO);
                  with co.IntervOrar[co.nIO-1] do
                    begin
                      Disciplina:=disc; Profesor:=xprof; Sala:=xsala; Tip:=xtip; Culoare:=HTMLtoTColor(col);
                      deLa := rdt + t1; panaLa := rdt + t2
                    end
                end;
              result:=true
            end
        end
    except result:=false; exit end
  end;
var phz: string; list: TStringList; i, j, k: integer; auxSpt: TSaptamana; auxIO: TIntervOrar;
begin
  try
    result:=false; phz:='initializing';
    try errL.Clear except errL:=TStringList.Create; errL.Sorted:=false; errL.Duplicates:=dupAccept end; errL.Clear;
    with co do begin nS:=0; nIO:=0; setlength(Saptamani, nS); setlength(IntervOrar, nIO) end;
    if not fileexists(fileName) then errL.Add('File "'+fileName+'" does not exist')
    else
      begin
        phz:='loading xml'; co.XML:=TXMLverySimple.Create; co.XML.LoadFromFile(fileName);
        phz:='checking for big nodes';
        if co.XML.Root.Find('Informatii')<>nil then co.Info:=co.XML.Root.Find('Informatii') else errL.Add('Node "Informatii" does not exist');
        if co.XML.Root.Find('Saptamani')<>nil then co.Sapt:=co.XML.Root.Find('Saptamani') else errL.Add('Node "Saptamani" does not exist');
        if co.XML.Root.Find('Orar')<>nil then co.Orar:=co.XML.Root.Find('Orar') else errL.Add('Node "Orar" does not exist');
        if errL.Count=0 then
          begin
            phz:='checking "Informatii->denumire" node';
            // Informatii check
            if co.Info.Find('denumire')=nil then errL.Add('In node "Informatii", node "denumire" does not exist')
            else with co.Info.Find('denumire') do
              if (not HasAttribute('universitate')) or (not HasAttribute('facultate')) or (not HasAttribute('grupa')) then
                errL.Add('In node "Informatii->denumire", one of the attributes "universitate", "facultate", "grupa" do not exist');
            phz:='checking "Informatii->valabil" node';
            if co.Info.Find('valabil')=nil then errL.Add('In node "Informatii", node "valabil" does not exist')
            else with co.Info.Find('valabil') do
              if (not HasAttribute('anUniversitar')) or (not HasAttribute('semestru')) then
                errL.Add('In node "Informatii->valabil", one of the attributes "anUniversitar", "semestru" do not exist');
            if errL.Count=0 then
              begin
                phz:='saptamani';
                // Saptamani check & decode
                if co.Sapt.ChildNodes.Count=0 then errL.Add('There are no week sets')
                else for i:=0 to co.Sapt.ChildNodes.Count-1 do with co.Sapt.ChildNodes[i] do
                  begin
                    phz:='week set i='+inttostr(i);
                    if (not HasAttribute('nume')) or (not HasAttribute('numeLung')) or (not HasAttribute('incepand')) then
                      begin errL.Add('In node "Saptamani->(nodul nr. '+inttostr(i)+')", one of the attributes "nume", "numeLung", "incepand" do not exist'); exit end;
                    phz:='decoding "incepand" list for week set "'+Attribute['nume']+'"';
                    DecodeList(Attribute['incepand'], '; ', list);
                    if list.Count>0 then for j:=0 to list.Count-1 do
                      begin
                        phz:='filling co.Saptamani with weeks from set "'+Attribute['nume']+'", j='+inttostr(j);
                        if PosInSaptamani(co.Saptamani, DecodeMyKindOfDate(list[j]))<>-1 then
                          begin
                            co.Saptamani[PosInSaptamani(co.Saptamani, DecodeMyKindOfDate(list[j]))].NumeSeturi.Add(Attribute['nume']);
                            //showmessagefmt('found list[%d]="%s", attr="%s" at pos=%d', [j, list[j], Attribute['nume'], PosInSaptamani(co.Saptamani, DecodeMyKindOfDate(list[j]))]);
                          end
                        else
                          begin
                            inc(co.nS); setlength(co.Saptamani, co.nS);
                            with co.Saptamani[co.nS-1] do
                              begin        //showmessagefmt('adding list[%d]="%s", attr="%s"', [j, list[j], Attribute['nume']]);
                                numeSeturi:=TStringList.Create; numeSeturi.Sorted:=false; numeSeturi.Duplicates:=dupIgnore;
                                incepandLuni:=DecodeMyKindOfDate(list[j]); numeSeturi.Add(Attribute['nume'])
                              end
                          end
                      end;
                    phz:='sorting co.Saptamani';
                    if length(co.Saptamani)>1 then for j:=0 to length(co.Saptamani)-2 do for k:=j+1 to length(co.Saptamani)-1 do
                      if co.Saptamani[j].incepandLuni>co.Saptamani[k].incepandLuni then
                        begin auxSpt:=co.Saptamani[j]; co.Saptamani[j]:=co.Saptamani[k]; co.Saptamani[k]:=auxSpt end;
                  end;
                      //assignfile(f, 'temp.txt'); rewrite(f); for i:=0 to length(co.Saptamani)-1 do //showmessagefmt('i=%d, "%s"', [i, co.Saptamani[i].NumeSeturi[0]]);
                      //begin write(f, EncodeMyKindOfDate(co.Saptamani[i].incepandLuni), ' - ', co.Saptamani[i].NumeSeturi.Count, ':'); for j:=0 to co.Saptamani[i].NumeSeturi.Count-1 do write(f, ' ', co.Saptamani[i].NumeSeturi[j]); writeln(f) end;
                // Orar check
                if co.Orar.ChildNodes.Count=0 then errL.Add('There are no disciplines in node "Orar"')
                else for i:=0 to co.Orar.ChildNodes.Count-1 do with co.Orar.ChildNodes[i] do
                  begin
                    if (not HasAttribute('nume')) or (not HasAttribute('col')) then begin errL.Add('In node "Orar->(discipl. nr. '+inttostr(i)+')", one of the attributes "nume", "col" do not exist'); exit end;
                    if ChildNodes.Count=0 then errL.Add('There are no items for discipline i='+inttostr(i))
                    else for j:=0 to ChildNodes.Count-1 do with ChildNodes[j] do
                      if (not HasAttribute('tip')) or (not HasAttribute('cand')) or (not HasAttribute('cu')) or (not HasAttribute('unde')) then
                        errL.Add('In node "Orar->(discipl. nr. '+inttostr(i)+')->(item nr. '+inttostr(j)+')", one of the attributes "tip", "cand", "cu", "unde" do not exist');
                  end;
                if errL.Count=0 then
                  begin
                    // Orar decode
                    for i:=0 to co.Orar.ChildNodes.Count-1 do with co.Orar.ChildNodes[i] do
                      begin
                        //showmessagefmt('disciplina=%s, col=%s', [Attribute['nume'], Attribute['col']]);
                        for j:=0 to ChildNodes.Count-1 do
                          if not AddIntervalOrar(co, Attribute['nume'], Attribute['col'], ChildNodes[j]) then
                            begin errL.Add(format('Failed to add Interval Orar for discipline i=%d="%s", item j=%d%sProbably the "cand" attribute is incorrect', [i, Attribute['name'], j, dnl])); exit end;
                        //showmessagefmt('co.IntervOrar[co.nIO-1=%d]: {disc=%s, deLa=%s, panaLa=%s}', [co.nIO-1, co.IntervOrar[co.nIO-1].Disciplina, formatdatetime('yyyy.mm.dd hh:nn', co.IntervOrar[co.nIO-1].deLa), formatdatetime('yyyy.mm.dd hh:nn', co.IntervOrar[co.nIO-1].panaLa)])
                      end;
                    for i:=0 to co.nIO-2 do for j:=i+1 to co.nIO-1 do
                      if co.IntervOrar[i].deLa>co.IntervOrar[j].deLa then
                        begin auxIO:=co.IntervOrar[i]; co.IntervOrar[i]:=co.IntervOrar[j]; co.IntervOrar[j]:=auxIO end;
                    co.maxIOpeZi:=1;
                    for i:=0 to co.nIO-2 do
                      begin
                        if co.IntervOrar[i].panaLa>co.IntervOrar[i+1].deLa then
                          errL.Add(format('Intervalele i=%d (%s) si j=%d (%s) se suprapun (de la %s)', [i, co.IntervOrar[i].Disciplina, j, co.IntervOrar[j].Disciplina, formatdatetime('d/mmm/yyyy', co.IntervOrar[j].deLa)]));
                        if trunc(co.IntervOrar[i].deLa)=trunc(co.IntervOrar[i+1].deLa) then inc(co.maxIOpeZi) else co.maxIOpeZi:=1
                      end;
                    result:=true;
                    FMain.WriteCourseList1Click(FMain)
                  end
              end
          end
      end
  except on E:Exception do begin
    result:=false; errL.Add('Unexpected exception: phase="'+phz+'"');
    MessageDlg(format('LoadOrar(fn="%s") ERROR:'+dnl+'%s - %s'+dnl+'Phase: '+phz, [fileName, E.ClassName, E.Message]), mtError, [mbOK], 0)
  end end
end;

function LoadSettings(var errL: TStringList): boolean;
var phz: string; xSett: TXMLVerySimple;
begin
  result:=false;
  try
    phz:='init tstringlist'; try errL.Clear except errL:=TStringList.Create; errL.Sorted:=false; errL.Duplicates:=dupAccept end; errL.Clear;
    phz:='init sett xml'; xSett:=TXMLVerySimple.Create;
    phz:='loading from file'; xSett.LoadFromFile(GetCurrentDir+'\data\settings.xml');
    phz:='getting lastUsedProject'; Sett.lastUsedProject:=xSett.Root.Find('lastUsedProject').Attribute['name'];
    phz:='getting dayCount'; Sett.dayCount:=strtoint(xSett.Root.Find('dayCount').Attribute['value']);
    phz:='getting ioBoxHeight'; Sett.ioBoxHeight:=strtoint(xSett.Root.Find('ioBoxHeight').Attribute['value']);
    //
    if not (Sett.dayCount in [3, 5, 7]) then Sett.dayCount:=5;
    if not (Sett.ioBoxHeight in [ioBoxHeight_AUTO_SEM, ioBoxHeight_AUTO_SEL]) then Sett.ioBoxHeight:=ioBoxHeight_AUTO_SEM;
    //
    TMenuItem(FMain.FindComponent('zileAfisate'+inttostr(Sett.dayCount))).Checked:=true;
    case Sett.ioBoxHeight of
    ioBoxHeight_AUTO_SEM: FMain.ioH_AUTO_SEM.Checked:=true;
    ioBoxHeight_AUTO_SEL: FMain.ioH_AUTO_SEL.Checked:=true;
    end;
    //
    result:=errL.Count=0
  except errL.Add('exited LoadSettings() on phase="'+phz+'"'); try xSett.Free finally end; exit end;
  try xSett.Free finally end;
  if result then SaveSettings
end;

function SaveSettings: boolean;
var xSett: TXMLVerySimple;
begin
  try
    result:=false;
    if not copyfile(pwidechar(GetCurrentDir+'\data\settings.xml'), pwidechar(GetCurrentDir+'\data\backups\settings '+formatdatetime(' yyyy-mm-dd hh.nn', now)+'.xml'), false) then
      if MessageDlg('Settings file backup (before save) has failed. Continue?', mtWarning, mbYesNo, 0)<>mrYes then exit;
    xSett:=TXMLVerySimple.Create;
    with xSett.Root do
      begin
        NodeName:='OFS2_Settings'; SetAttribute('lastSaved', formatdatetime('d mmmm yyyy, h:nn:ss', now));
        AddChild('lastUsedProject'); ChildNodes.Last.SetAttribute('name', Sett.lastUsedProject);
        AddChild('dayCount');        ChildNodes.Last.SetAttribute('value', inttostr(Sett.dayCount));
        AddChild('ioBoxHeight');     ChildNodes.Last.SetAttribute('value', inttostr(Sett.ioBoxHeight));
      end;
    xSett.SaveToFile(GetCurrentDir+'\data\settings.xml'); result:=true; try xSett.Free finally end
  except on E:Exception do begin
    MessageDlg('SaveSettings ERROR: '+E.ClassName+' - '+E.Message+dnl+'The program will continue (but the settings have not been saved).', mtError, [mbIgnore], 0);
    result:=false; try xSett.Free finally end
  end end
end;

function InitializationOK(var errL: TStringList): boolean;
var i: word; fonts: TStringList;
begin
  result:=false;
  try
    try errL.Clear except errL:=TStringList.Create; errL.Sorted:=false; errL.Duplicates:=dupAccept end; errL.Clear;
    with FormatSettings do begin DecimalSeparator:='.'; ThousandSeparator:=','; DateSeparator:='/'; TimeSeparator:=':' end;
    for i:=low(NFo) to high(NFo) do
      if not directoryexists(NFo[i]) then
        begin errL.Add('Folder "'+NFo[i]+'" does not exist'); CreateDir(NFo[i]) end;
    for i:=low(NFi) to high(NFi) do
      if not fileexists(NFi[i]) then
        errL.Add('File "'+NFi[i]+'" does not exist');
    assignfile(f, 'data\log.txt'); rewrite(f); closefile(f);
    // Fonts
    fonts:=TStringList.Create; fonts.Delimiter:=';'; fonts.QuoteChar:='|';
    fonts.DelimitedText:='|Signika|;|Ubuntu|;|Ubuntu Mono|'; //showmessagefmt('count=%d', [fonts.Count]);
    if not FontCheck(fonts) then
      begin
        fonts.Clear; ScanForFiles(GetCurrentDir+'\files\fonts\', 'ttf', fonts); //showmessagefmt('something missing; scanned=%d', [fonts.Count]);
          for i:=0 to fonts.Count-1 do
            begin
              AddFontResource(PWideChar(GetCurrentDir+'\files\fonts\'+fonts[i]));
              SendMessage(HWND_BROADCAST,WM_FONTCHANGE,0,0)
            end;
        MessageDlg('Some fonts were missing and I think they have been installed. If not, do so manually please.', mtInformation, [mbOK], 0)
      end;
    //
    result:=errL.Count=0
  except exit end
end;

end.

