unit Calendar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TFCalendar = class(TForm)
    Image1: TImage;
    cal: TMonthCalendar;
    titL: TLabel;
    LoadB: TImage;
    CancelB: TImage;
    subtitL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CancelBClick(Sender: TObject);
    procedure calClick(Sender: TObject);
    procedure LoadBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCalendar: TFCalendar;

implementation

{$R *.dfm}

uses Main, FunctiiBaza, DataTypes, VCLCustomMethods;

procedure TFCalendar.calClick(Sender: TObject);
begin
  subtitL.Caption:=Format('Vrei sa mergi la %s?', [DataInRomana(cal.Date)])
end;

procedure TFCalendar.CancelBClick(Sender: TObject);
begin
  FCalendar.Close
end;

procedure TFCalendar.FormCreate(Sender: TObject);
begin
  ColorImagePanel(Image1, colForm, colButtonMouseOver);
  CancelB.OnMouseEnter:=FMain.ButtonIMGMouseEnter; LoadB.OnMouseEnter:=FMain.ButtonIMGMouseEnter;
  CancelB.OnMouseLeave:=FMain.ButtonIMGMouseLeave; LoadB.OnMouseLeave:=FMain.ButtonIMGMouseLeave;
  CancelB.OnMouseLeave(CancelB); LoadB.OnMouseLeave(LoadB)
end;

procedure TFCalendar.LoadBClick(Sender: TObject);
begin
  centerDate:=cal.Date;
  CancelBClick(LoadB);
  FMain.RefreshOrar1Click(LoadB)
end;

end.
