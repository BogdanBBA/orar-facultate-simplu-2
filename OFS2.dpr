program OFS2;

uses
  Vcl.Forms,
  Main in 'Main.pas' {FMain},
  VCLCustomMethods in 'VCLCustomMethods.pas',
  ULoadOrar in 'ULoadOrar.pas' {FLoadOrar},
  Cursuri in 'Cursuri.pas' {FCurs},
  About in 'About.pas' {FAbout},
  Calendar in 'Calendar.pas' {FCalendar};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMain, FMain);
  Application.CreateForm(TFLoadOrar, FLoadOrar);
  Application.CreateForm(TFCurs, FCurs);
  Application.CreateForm(TFAbout, FAbout);
  Application.CreateForm(TFCalendar, FCalendar);
  Application.Run;
end.
