unit VCLCustomMethods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellAPI, Menus, ExtCtrls, jpeg, PngImage, ImgList,
  ComCtrls, ToolWin, UIntList, DateUtils, DataTypes;

const
  colForm=$003C3933;
  colButtonNormal=$004E4A43;
  colButtonMouseOver=$006F6960;
  colTitleImgNormal=$004B4741;
  colTitleImgMouseOver=$00615F49;

  colGrayedText=$00D8D1CB;

  clOrange=$0000B3FF;

var
  tw, th: integer;

procedure prevImageMouseOver; //uses FMain elements
procedure prevImageMouseLeave; //uses FMain elements
procedure nextImageMouseOver; //uses FMain elements
procedure nextImageMouseLeave; //uses FMain elements
procedure RefreshImageButton(var butt: TImage; penCol, fillCol: TColor; text: string; font: TFont);
procedure RefreshTitleImg(var img: TImage; fillCol: TColor; aDate: TDate; font, dateFont: TFont);
procedure ColorImagePanel(var img: TImage; bgCol, borderCol: TColor); //generic
procedure RefreshIOImage(var img: TImage; io: TIntervOrar; fillCol: TColor); // Uses FMain elements

implementation

uses Main;

procedure prevImageMouseOver;
begin
  with FMain.prevI do
    begin
      Picture:=nil; Canvas.Brush.Style:=bsSolid; Canvas.Pen.Style:=psSolid;
      Canvas.Brush.Color:=colButtonMouseOver; Canvas.FillRect(Rect(0, 0, Width+2, Height+2));
      Canvas.Pen.Color:=clBlack; Canvas.Rectangle(0, 0, Width, Height);
      Canvas.MoveTo(3*Width div 4, 0); Canvas.LineTo(3*Width div 4, Height);
      Canvas.Font:=FMain.Label12.Font; Canvas.Font.Size:=40; tw:=Canvas.TextWidth('�'); th:=Canvas.TextHeight('�');
      Canvas.TextOut((3*Width div 4) div 2-tw div 2, Height div 2-th div 2, '�');
      Canvas.Font.Size:=22; tw:=Canvas.TextWidth('+'); th:=Canvas.TextHeight('+');
      Canvas.TextOut(3*Width div 4+(Width div 4) div 2-tw div 2, Height div 2-th div 2, '+')
    end
end;

procedure prevImageMouseLeave;
begin
  with FMain.prevI do
    begin
      Picture:=nil; Canvas.Brush.Style:=bsSolid; Canvas.Pen.Style:=psSolid;
      Canvas.Brush.Color:=colButtonNormal; Canvas.FillRect(Rect(0, 0, Width+2, Height+2));
      Canvas.Pen.Color:=clBlack; Canvas.Rectangle(0, 0, Width, Height);
      Canvas.MoveTo(3*Width div 4, 0); Canvas.LineTo(3*Width div 4, Height);
      Canvas.Font:=FMain.Label12.Font; Canvas.Font.Size:=40; tw:=Canvas.TextWidth('�'); th:=Canvas.TextHeight('�');
      Canvas.TextOut((3*Width div 4) div 2-tw div 2, Height div 2-th div 2, '�');
      Canvas.Font.Size:=22; tw:=Canvas.TextWidth('+'); th:=Canvas.TextHeight('+');
      Canvas.TextOut(3*Width div 4+(Width div 4) div 2-tw div 2, Height div 2-th div 2, '+')
    end
end;

procedure nextImageMouseOver;
begin
  with FMain.nextI do
    begin
      Picture:=nil; Canvas.Brush.Style:=bsSolid; Canvas.Pen.Style:=psSolid;
      Canvas.Brush.Color:=colButtonMouseOver; Canvas.FillRect(Rect(0, 0, Width+2, Height+2));
      Canvas.Pen.Color:=clBlack; Canvas.Rectangle(0, 0, Width, Height);
      Canvas.MoveTo(Width div 4, 0); Canvas.LineTo(Width div 4, Height);
      Canvas.Font:=FMain.Label12.Font; Canvas.Font.Size:=40; tw:=Canvas.TextWidth('�'); th:=Canvas.TextHeight('�');
      Canvas.TextOut(Width div 4+(3*Width div 4) div 2-tw div 2, Height div 2-th div 2, '�');
      Canvas.Font.Size:=22; tw:=Canvas.TextWidth('+'); th:=Canvas.TextHeight('+');
      Canvas.TextOut((Width div 4) div 2-tw div 2, Height div 2-th div 2, '+')
    end
end;

procedure nextImageMouseLeave;
begin
  with FMain.nextI do
    begin
      Picture:=nil; Canvas.Brush.Style:=bsSolid; Canvas.Pen.Style:=psSolid;
      Canvas.Brush.Color:=colButtonNormal; Canvas.FillRect(Rect(0, 0, Width+2, Height+2));
      Canvas.Pen.Color:=clBlack; Canvas.Rectangle(0, 0, Width, Height);
      Canvas.MoveTo(Width div 4, 0); Canvas.LineTo(Width div 4, Height);
      Canvas.Font:=FMain.Label12.Font; Canvas.Font.Size:=40; tw:=Canvas.TextWidth('�'); th:=Canvas.TextHeight('�');
      Canvas.TextOut(Width div 4+(3*Width div 4) div 2-tw div 2, Height div 2-th div 2, '�');
      Canvas.Font.Size:=22; tw:=Canvas.TextWidth('+'); th:=Canvas.TextHeight('+');
      Canvas.TextOut((Width div 4) div 2-tw div 2, Height div 2-th div 2, '+')
    end
end;

procedure RefreshImageButton(var butt: TImage; penCol, fillCol: TColor; text: string; font: TFont);
var tw, th: integer;
begin
  butt.Picture:=nil;
  butt.Canvas.Brush.Color:=fillCol; butt.Canvas.Brush.Style:=bsSolid;
  butt.Canvas.Pen.Color:=penCol; butt.Canvas.Pen.Style:=psSolid; butt.Canvas.Pen.Width:=1;
  butt.Canvas.Rectangle(0, 0, butt.Width, butt.Height);
  butt.Canvas.FillRect(Rect(1, 1, butt.Width-2, butt.Height-2));
  butt.Canvas.Font:=font;
  tw:=butt.Canvas.TextWidth(text); th:=butt.Canvas.TextHeight(text);
  butt.Canvas.TextOut(butt.Width div 2-tw div 2, butt.Height div 2-th div 2, text)
end;

procedure RefreshTitleImg(var img: TImage; fillCol: TColor; aDate: TDate; font, dateFont: TFont);
var top: integer; text: string;
begin
  img.Picture:=nil;
  img.Canvas.Brush.Color:=fillCol; img.Canvas.Brush.Style:=bsSolid;
  img.Canvas.FillRect(Rect(0, 0, img.Width, img.Height));

  img.Canvas.Font:=font; text:=ZiSaptS[DayOfTheWeek(aDate)];
  tw:=img.Canvas.TextWidth(text); th:=img.Canvas.TextHeight(text);
  top:=img.Height div 8;
  img.Canvas.TextOut(img.Width div 2-tw div 2, top, text);

  img.Canvas.Font:=dateFont; text:=formatdatetime('d "'+LunaAnului[strtoint(formatdatetime('m', aDate))]+'" yyyy', aDate);
  tw:=img.Canvas.TextWidth(text); th:=img.Canvas.TextHeight(text);
  top:=img.Height-th-8;
  img.Canvas.TextOut(img.Width div 2-tw div 2, top, text);

  if trunc(aDate)=trunc(now) then
    begin
      img.Canvas.Brush.Color:=clOrange;
      img.Canvas.FillRect(Rect(0, img.Height-4, img.Width, img.Height));
    end;
end;

procedure ColorImagePanel(var img: TImage; bgCol, borderCol: TColor);
begin
  img.Picture:=nil;
  img.Canvas.Brush.Style:=bsSolid; img.Canvas.Brush.Color:=bgCol;
  img.Canvas.FillRect(Rect(0, 0, img.Width+2, img.Height+2));

  img.Canvas.Pen.Style:=psSolid; img.Canvas.Pen.Color:=borderCol; img.Canvas.Pen.Width:=16;
  img.Canvas.Rectangle(0, 0, img.Width+2, img.Height+2)
end;

procedure RefreshIOImage(var img: TImage; io: TIntervOrar; fillCol: TColor);
var left: word; xh: array[0..2] of word;
begin
  img.Picture:=nil;
  img.Canvas.Brush.Color:=fillCol; img.Canvas.Brush.Style:=bsSolid;
  img.Canvas.FillRect(Rect(0, 0, img.Width, img.Height));

  img.Canvas.Brush.Color:=io.Culoare;
  //if (io.Disciplina[1]='[') and (io.Disciplina[length(io.Disciplina)]=']') then img.Canvas.Brush.Color:=clWhite;
  img.Canvas.FillRect(Rect(0, 0, img.Width div 20, img.Height));

  img.Canvas.Brush.Color:=fillCol;
  left:=img.Width div 8;

  img.Canvas.Font:=FMain.Label14.Font; xh[0]:=img.Canvas.TextHeight('AptqfWy');
  if (io.Disciplina[1]='[') and (io.Disciplina[length(io.Disciplina)]=']') then
  begin img.Canvas.Font.Color:=colGrayedText; img.Canvas.TextOut(left, 10, copy(io.Disciplina, 2, length(io.Disciplina)-2)) end
  else img.Canvas.TextOut(left, 10, io.Disciplina);

  img.Canvas.Font:=FMain.Label15.Font; xh[1]:=img.Canvas.TextHeight('AptqfWy');
  img.Canvas.TextOut(left, 10+xh[0], ansiuppercase(io.Tip));

  img.Canvas.Font:=FMain.Label16.Font;
  img.Canvas.TextOut(left, 10+xh[0]+xh[1], Format('%s - %s', [formatdatetime('h:nn', io.deLa), formatdatetime('h:nn', io.panaLa)]));
end;

end.
