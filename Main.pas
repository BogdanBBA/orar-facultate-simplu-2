unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, XML.VerySimple, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Menus, Vcl.Imaging.pngimage,
  DateUtils, Vcl.ComCtrls;

type
  TFMain = class(TForm)
    CloseB: TImage;
    Panel1: TPanel;
    ButtonIMG: TImage;
    Label1: TLabel;
    Shape1: TShape;
    MainMenu1: TMainMenu;
    Aplicaie1: TMenuItem;
    Despre1: TMenuItem;
    Ieire1: TMenuItem;
    Opiuni1: TMenuItem;
    Editareorare1: TMenuItem;
    MetodeAscunse1: TMenuItem;
    IncarcaOrar1: TMenuItem;
    RefreshOrar1: TMenuItem;
    Orar1: TMenuItem;
    Zileafiate1: TMenuItem;
    zileAfisate3: TMenuItem;
    zileAfisate5: TMenuItem;
    zileAfisate7: TMenuItem;
    InaltimeCasute1: TMenuItem;
    logoImg: TImage;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    titleImage: TImage;
    Label12: TLabel;
    Label13: TLabel;
    ioH_AUTO_SEM: TMenuItem;
    ioH_AUTO_SEL: TMenuItem;
    IOImage: TImage;
    nextDatePM: TPopupMenu;
    N1day1: TMenuItem;
    N3days1: TMenuItem;
    N1week1: TMenuItem;
    N1month1: TMenuItem;
    prevDatePM: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Firstday1: TMenuItem;
    Lastday1: TMenuItem;
    Dumla1: TMenuItem;
    Ceamaiapropriatzidecurs1: TMenuItem;
    Ultimazidinsemestru1: TMenuItem;
    Primazidinsemestru1: TMenuItem;
    Ziuacuurmtorulcurs1: TMenuItem;
    LoadOrarMenuItem: TMenuItem;
    Label17: TLabel;
    WriteCourseList1: TMenuItem;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Sptmncentrat57zile1: TMenuItem;
    nextI: TImage;
    prevI: TImage;
    Ziuadeazi1: TMenuItem;
    Ozianume1: TMenuItem;
    procedure CloseBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonIMGMouseEnter(Sender: TObject);
    procedure ButtonIMGMouseLeave(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure zileAfisate3Click(Sender: TObject);
    procedure RefreshOrar1Click(Sender: TObject);
    procedure titleImageMouseEnter(Sender: TObject);
    procedure titleImageMouseLeave(Sender: TObject);
    procedure titleImageClick(Sender: TObject);
    procedure Despre1Click(Sender: TObject);
    procedure ioH_AUTO_SEMClick(Sender: TObject);
    procedure IOImageMouseLeave(Sender: TObject);
    procedure IOImageMouseEnter(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure N1day1Click(Sender: TObject);
    procedure N3days1Click(Sender: TObject);
    procedure N1week1Click(Sender: TObject);
    procedure N1month1Click(Sender: TObject);
    procedure Firstday1Click(Sender: TObject);
    procedure Lastday1Click(Sender: TObject);
    procedure Ceamaiapropriatzidecurs1Click(Sender: TObject);
    procedure Ziuacuurmtorulcurs1Click(Sender: TObject);
    procedure LoadOrarMenuItemClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WriteCourseList1Click(Sender: TObject);
    procedure nextIMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure nextIMouseLeave(Sender: TObject);
    procedure nextIClick(Sender: TObject);
    procedure prevIClick(Sender: TObject);
    procedure prevIMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure prevIMouseLeave(Sender: TObject);
    procedure IOImageClick(Sender: TObject);
    procedure Ziuadeazi1Click(Sender: TObject);
    procedure Ozianume1Click(Sender: TObject);
  private
    procedure InitializeDGs;
    procedure ArrangeLabelsHorizontally(a: array of TLabel);
  public
    { Public declarations }
  end;

type TDayGroup=record
  Panel: TPanel;
  titleI: TImage;
  ioI: array of TImage;
end;

var
  FMain: TFMain;
  dg: array[1..7] of TDayGroup;
  failedToLoadLastUsedOrar: boolean;
  prevIPos, nextIPos: TPoint;

implementation

{$R *.dfm}

uses DataTypes, FunctiiBaza, VCLCustomMethods, ULoadOrar, Cursuri, About, Calendar;

procedure TFMain.RefreshOrar1Click(Sender: TObject);
var i, j, kIO, kIOi: integer; nio: byte; d: TDate; TR1, TR2: TTime;
    ioTopP, ioHeightP: real; ioTop, ioHeight: word;
begin
  if CurrentlyClosing then exit;
  Label3.Caption:=Orar.Info.Find('denumire').Attribute['universitate'];
  Label5.Caption:=Orar.Info.Find('denumire').Attribute['facultate'];
  Label7.Caption:=Orar.Info.Find('denumire').Attribute['grupa'];
  Label9.Caption:=Orar.Info.Find('valabil').Attribute['anUniversitar'];
  Label11.Caption:=Orar.Info.Find('valabil').Attribute['semestru'];
  Label18.Caption:=DataInRomana(Orar.IntervOrar[0].deLa);
  Label20.Caption:=DataInRomana(Orar.IntervOrar[Orar.nIO-1].panaLa);
  FMain.ArrangeLabelsHorizontally([Label2, Label3]);
  FMain.ArrangeLabelsHorizontally([Label4, Label5, Label6, Label7]);
  FMain.ArrangeLabelsHorizontally([Label8, label9, Label10, Label11]);
  FMain.ArrangeLabelsHorizontally([Label17, Label18, label19, label20]);
  // Resizing panels, (creating and) resizing IO images
  try
  for i:=1 to 7 do
    if i<=Sett.dayCount then
      with dg[i] do
        begin
          Panel.Visible:=true;
          Panel.Top:=closeB.Height+2*closeB.Top; Panel.Height:=FMain.Height-Panel.Top-24;
          Panel.Left:=trunc((FMain.Width/Sett.dayCount)*(i-1)); Panel.Width:=trunc(FMain.Width/Sett.dayCount)+1;
          //
          titleI.Left:=0; titleI.Top:=0; titleI.Width:=Panel.Width; titleI.Height:=80;
          titleImageMouseLeave(titleI);
          //
          d:=GetDateForOrarTitleI(TImage(titleI)); kIO:=FirstIOPoz(Orar, d); nio:=nIOforDate(Orar, d);
          if length(ioI)>nio then for j:=nio to length(ioI)-1 do if ioI[j]<>nil then ioI[j].Free;
          Panel.Refresh; SetLength(ioI, nio);
          //
          if nIO>0 then
            for j:=0 to nio-1 do
              begin
                if ioI[j]=nil then
                  with Orar.IntervOrar[kIO] do
                    begin
                      ioI[j]:=TImage.Create(Self); ioI[j].Parent:=Panel; ioI[j].Left:=0;
                      ioI[j].Hint := Format('%d:%s/%s-%s', [kIO, EncodeMyKindOfDate(d), EncodeMyKindOfTime(deLa), EncodeMyKindOfTime(panaLa)]);
                      ioI[j].OnClick:=IOImageClick; ioI[j].OnMouseLeave:=IOImageMouseLeave; ioI[j].OnMouseEnter:=IOImageMouseEnter;
                      IOImageMouseLeave(ioI[j]); inc(kIO)
                    end;
                ioI[j].Width:=Panel.Width
              end
        end
    else dg[i].Panel.Hide
  except on E:Exception do MessageDlg(Format('RefreshOrar/Section1 (Resizing) ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end;
  //
  GetCurrentTimeRange(Orar, Sett.ioBoxHeight, TR1, TR2); //showmessagefmt('Large IO: %s - %s', [formatdatetime('h:nn', tr1), formatdatetime('h:nn', tr2)]);
  // Updating data for IO images
  try
  for i:=1 to Sett.dayCount do
    if length(dg[i].ioI)>0 then
      with dg[i] do
        begin
          d:=GetDateForOrarTitleI(titleI);
          kIO:=FirstIOPoz(Orar, d); kIOi:=0;
          while trunc(Orar.IntervOrar[kIO].deLa)=trunc(d) do
            begin
              ioI[kIOi].Hint:=Format('%d:%s/%s-%s', [kIO, EncodeMyKindOfDate(d), EncodeMyKindOfTime(Orar.IntervOrar[kIO].deLa), EncodeMyKindOfTime(Orar.IntervOrar[kIO].panaLa)]);
              GetIntervalPercentage(Orar.IntervOrar[kIO].deLa, Orar.IntervOrar[kIO].panaLa, TR1, TR2, ioTopP, ioHeightP);
              ioTop := titleI.Height + round((Panel.Height-titleI.Height) * ioTopP / 100);
              ioHeight := round((Panel.Height-titleI.Height) * ioHeightP / 100);
              //showmessagefmt('day = %s%sdisciplina: %s%sio: %s-%s (large io: %s - %s)%spanel.height = %d%susable height = %d%stop = %f%% = %dpx%sheight = %f%% = %dpx', [formatdatetime('d mmmm yyyy', Orar.IntervOrar[kIO].deLa), dnl, Orar.IntervOrar[kIO].Disciplina, dnl, formatdatetime('h:nn', Orar.IntervOrar[kIO].deLa), formatdatetime('h:nn', Orar.IntervOrar[kIO].panaLa), formatdatetime('h:nn', TR1), formatdatetime('h:nn', TR2), dnl, Panel.Height, nl, Panel.Height-titleI.Height, nl, ioTopP, iotop, nl, ioHeightP, ioheight]);
              ioI[kIOi].Top:=ioTop; ioI[kIOi].Height:=ioHeight;
              RefreshIOImage(ioI[kIOi], Orar.IntervOrar[kIO], colForm);
              inc(kIO); inc(kIOi)
            end
        end
  except on E:Exception do MessageDlg(Format('RefreshOrar/Section2 (IO img data update) ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end
end;

procedure TFMain.InitializeDGs;
var i: word;
begin
  try
  for i:=1 to 7 do
    with dg[i] do
      begin
        Panel:=TPanel.Create(Self); Panel.Parent:=FMain; Panel.BevelOuter:=bvNone; Panel.Visible:=false;
        titleI:=TImage.Create(Self); titleI.Parent:=Panel; titleI.Cursor:=crHandPoint;
        titleI.OnClick:=titleImageClick; titleI.OnMouseEnter:=titleImageMouseEnter; titleI.OnMouseLeave:=titleImageMouseLeave
      end
  except on E:Exception do MessageDlg(Format('InitializeDGs ERROR: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0) end;
  CompletelyInitialized:=true
end;

//

procedure TFMain.ArrangeLabelsHorizontally(a: array of TLabel);
var i: word;
begin
  for i:=low(a)+1 to high(a) do
    a[i].Left := a[i-1].Left + a[i-1].Width + 6
end;

procedure TFMain.FormCreate(Sender: TObject);
var errL: TStringList;
begin
  if not InitializationOK(errL) then
    begin
      ShowErrorList(mtError, 'Program initialization failed. Here is an error log:', 'The program will close.', errL);
      Halt
    end;
  //
  if not LoadSettings(errL) then
    begin
      ShowErrorList(mtError, 'Could not load and correctly decode the settings file; it''s broken. Here is an error log:', 'Repair it manually (with your hands). The program will close.', errL);
      Halt
    end;
  //
  failedToLoadLastUsedOrar:=not LoadOrar(GetCurrentDir+'\data\'+Sett.lastUsedProject+'.xml', orar, errL);
  if failedToLoadLastUsedOrar then
    ShowErrorList(mtError, format('Orar "%s" (default) could not be loaded. Here is why:', [GetCurrentDir+'\data\'+Sett.lastUsedProject+'.xml']), 'The program will attempt to let you load another file. If this fails, please edit the settings file manually or seek help.', errL);
  //
  centerDate:=now;
  FMain.Color:=colForm;
  ButtonIMGMouseLeave(CloseB); prevImageMouseLeave; nextImageMouseLeave;
  FMain.InitializeDGs
end;

procedure TFMain.FormResize(Sender: TObject);
begin
  CloseB.Top:=16; CloseB.Left:=FMain.Width-CloseB.Width-16;
  nextI.Top:=CloseB.Top; nextI.Left:=CloseB.Left-nextI.Width-16;
  prevI.Top:=nextI.Top; prevI.Left:=nextI.Left-prevI.Width-16;
  FMain.RefreshOrar1Click(FMain)
end;

procedure TFMain.FormShow(Sender: TObject);
begin
  if failedToLoadLastUsedOrar then FMain.LoadOrarMenuItemClick(FMain)
  else Ceamaiapropriatzidecurs1Click(FMain)
end;

procedure TFMain.CloseBClick(Sender: TObject);
begin
  CurrentlyClosing:=true;
  FMain.Close
end;

procedure TFMain.Despre1Click(Sender: TObject);
begin FAbout.ShowModal end;

procedure TFMain.zileAfisate3Click(Sender: TObject);
const auxNZ: array[0..2] of byte=(3, 5, 7);
var i: word;
begin
  for i:=0 to Zileafiate1.Count-1 do if Sender=Zileafiate1.Items[i] then Sett.dayCount:=auxNZ[i];
  if SaveSettings then for i:=0 to Zileafiate1.Count-1 do Zileafiate1.Items[i].Checked:=Sender=Zileafiate1.Items[i];
  FMain.RefreshOrar1Click(FMain)
end;

procedure TFMain.ioH_AUTO_SEMClick(Sender: TObject);
var i: word;
begin
  if Sender=ioH_AUTO_SEM then Sett.ioBoxHeight:=ioBoxHeight_AUTO_SEM
  else if Sender=ioH_AUTO_SEL then Sett.ioBoxHeight:=ioBoxHeight_AUTO_SEL;
  if SaveSettings then for i:=0 to InaltimeCasute1.Count-1 do InaltimeCasute1.Items[i].Checked:=Sender=InaltimeCasute1.Items[i];
  FMain.RefreshOrar1Click(FMain)
end;

procedure TFMain.IOImageClick(Sender: TObject);
var x: word;
begin
  try x:=strtoint(copy(TImage(Sender).Hint, 1, pos(':', TImage(Sender).Hint)-1)) except exit end;
  FCurs.titL.Caption:=Orar.IntervOrar[x].Disciplina;
  FCurs.ShowModal
end;

procedure TFMain.IOImageMouseEnter(Sender: TObject);
var p: integer; sx: string;
begin
  try
    sx:=TImage(Sender).Hint; p:=strtoint(copy(sx, 1, pos(':', sx)-1));
    if p in [0..Orar.nIO-1] then
      RefreshIOImage(TImage(Sender), Orar.IntervOrar[p], colTitleImgNormal)
  except exit end
end;

procedure TFMain.IOImageMouseLeave(Sender: TObject);
var p: integer; sx: string;
begin
  try
    sx:=TImage(Sender).Hint; p:=strtoint(copy(sx, 1, pos(':', sx)-1));
    if p in [0..Orar.nIO-1] then
      RefreshIOImage(TImage(Sender), Orar.IntervOrar[p], colForm)
  except exit end
end;

procedure TFMain.Ziuacuurmtorulcurs1Click(Sender: TObject);
var x, k: integer;
begin
  if Orar.nIO=0 then exit;
  if now>Orar.IntervOrar[Orar.nIO-1].panaLa then begin MessageDlg('S-a terminat semestrul!', mtInformation, [mbOK], 0); exit end;
  k:=0; repeat x:=FirstIOPoz(Orar, incDay(trunc(now), k)); inc(k) until x<>-1;
  while Orar.IntervOrar[x].deLa<=now do inc(x);
  centerDate:=trunc(Orar.IntervOrar[x].deLa); FMain.RefreshOrar1Click(Ziuacuurmtorulcurs1)
end;

procedure TFMain.Ziuadeazi1Click(Sender: TObject);
begin centerDate:=trunc(now); FMain.RefreshOrar1Click(Ceamaiapropriatzidecurs1) end;

procedure TFMain.Ceamaiapropriatzidecurs1Click(Sender: TObject);
var d, dx: TDate; i: word;
begin
  if Orar.nIO=0 then exit;
  i:=1; d:=encodedate(1900, 1, 1);
  if FirstIOPoz(Orar, trunc(now))<>-1 then d:=trunc(now);
  while (i<65000) and (d=encodedate(1900, 1, 1)) do
    begin
      dx:=incDay(trunc(now),  i); if FirstIOPoz(Orar, dx)<>-1 then d:=dx;
      dx:=incDay(trunc(now), -i); if FirstIOPoz(Orar, dx)<>-1 then d:=dx;
      inc(i)
    end;
  if d<>encodedate(1900, 1, 1) then
    begin centerDate:=d; FMain.RefreshOrar1Click(Ceamaiapropriatzidecurs1) end
end;

procedure TFMain.MenuItem1Click(Sender: TObject);
begin centerDate:=incDay(centerDate, -1); FMain.RefreshOrar1Click(prevI) end;

procedure TFMain.MenuItem2Click(Sender: TObject);
begin centerDate:=incDay(centerDate, -3); FMain.RefreshOrar1Click(prevI) end;

procedure TFMain.MenuItem3Click(Sender: TObject);
begin centerDate:=incWeek(centerDate, -1); FMain.RefreshOrar1Click(prevI) end;

procedure TFMain.MenuItem4Click(Sender: TObject);
begin centerDate:=incMonth(centerDate, -1); FMain.RefreshOrar1Click(prevI) end;

procedure TFMain.Firstday1Click(Sender: TObject);
begin centerDate:=trunc(Orar.IntervOrar[0].deLa); FMain.RefreshOrar1Click(prevI) end;

procedure TFMain.N1day1Click(Sender: TObject);
begin centerDate:=incDay(centerDate); FMain.RefreshOrar1Click(nextI) end;

procedure TFMain.N3days1Click(Sender: TObject);
begin centerDate:=incDay(centerDate, 3); FMain.RefreshOrar1Click(nextI) end;

procedure TFMain.prevIClick(Sender: TObject);
begin
  if prevIPos.X<=3*prevI.Width div 4 then MenuItem3Click(prevI)
  else prevDatePM.Popup(prevI.Left+3*prevI.Width div 4, prevI.Top+prevI.Height+19)
end;

procedure TFMain.nextIClick(Sender: TObject);
begin
  if nextIPos.X>=nextI.Width div 4 then N1week1Click(nextI)
  else nextDatePM.Popup(nextI.Left, nextI.Top+nextI.Height+19)
end;

procedure TFMain.prevIMouseLeave(Sender: TObject);
begin prevImageMouseLeave end;

procedure TFMain.prevIMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin prevIPos.X:=X; prevIPos.Y:=Y; prevImageMouseOver end;

procedure TFMain.nextIMouseLeave(Sender: TObject);
begin nextImageMouseLeave end;

procedure TFMain.nextIMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin nextIPos.X:=X; nextIPos.Y:=Y; nextImageMouseOver end;

procedure TFMain.Ozianume1Click(Sender: TObject);
begin FCalendar.ShowModal end;

procedure TFMain.LoadOrarMenuItemClick(Sender: TObject);
begin FLoadOrar.ShowModal end;

procedure TFMain.N1week1Click(Sender: TObject);
begin centerDate:=incWeek(centerDate); FMain.RefreshOrar1Click(nextI) end;

procedure TFMain.N1month1Click(Sender: TObject);
begin centerDate:=incMonth(centerDate); FMain.RefreshOrar1Click(nextI) end;

procedure TFMain.Lastday1Click(Sender: TObject);
begin centerDate:=trunc(Orar.IntervOrar[Orar.nIO-1].deLa); FMain.RefreshOrar1Click(prevI) end;

procedure TFMain.ButtonIMGMouseEnter(Sender: TObject);
begin
  if Sender is TImage then
    RefreshImageButton(TImage(Sender), clBlack, colButtonMouseOver, TImage(Sender).Hint, Label1.Font)
end;

procedure TFMain.ButtonIMGMouseLeave(Sender: TObject);
begin
  if Sender is TImage then
    RefreshImageButton(TImage(Sender), clBlack, colButtonNormal, TImage(Sender).Hint, Label1.Font)
end;

procedure TFMain.titleImageClick(Sender: TObject);
begin
  centerDate:=GetDateForOrarTitleI(TImage(Sender)); FMain.RefreshOrar1Click(titleImage)
end;

procedure TFMain.titleImageMouseEnter(Sender: TObject);
begin
  if Sender is TImage then
    RefreshTitleImg(TImage(Sender), colTitleImgMouseOver, GetDateForOrarTitleI(TImage(Sender)), Label12.Font, Label13.Font)
end;

procedure TFMain.titleImageMouseLeave(Sender: TObject);
begin
  if Sender is TImage then
    RefreshTitleImg(TImage(Sender), colTitleImgNormal, GetDateForOrarTitleI(TImage(Sender)), Label12.Font, Label13.Font)
end;

procedure TFMain.WriteCourseList1Click(Sender: TObject);
const xs='================================================';
var i: integer; f: textfile;
begin
  assignfile(f, 'courseList.txt'); rewrite(f);
  writeln(f, Format('%s%sOrar pt %s / %s (%s), an %s, sem %s%s%s', [xs, nl, Orar.Info.Find('denumire').Attribute['universitate'], Orar.Info.Find('denumire').Attribute['facultate'], Orar.Info.Find('denumire').Attribute['grupa'], Orar.Info.Find('valabil').Attribute['anUniversitar'], Orar.Info.Find('valabil').Attribute['semestru'], nl, xs]));
  for i:=0 to orar.nIO-1 do
    with orar.IntervOrar[i] do
      writeln(f, format('%3d. deLa (%s), panaLa (%s), nume="%s"', [i, formatdatetime('ddd, dd/mmm/yyyy hh:nn', deLa), formatdatetime('ddd, dd/mmm/yyyy hh:nn', panaLa), Disciplina]));
  writeln(f, xs); closefile(f);
end;

end.
