object FCurs: TFCurs
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsNone
  Caption = 'FCurs'
  ClientHeight = 429
  ClientWidth = 770
  Color = 3946803
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Signika'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  ScreenSnap = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object Image1: TImage
    Left = 8
    Top = 8
    Width = 753
    Height = 413
  end
  object CloseB: TImage
    Left = 547
    Top = 24
    Width = 200
    Height = 75
    Cursor = crHandPoint
    Hint = #206'nchidere'
    OnClick = CloseBClick
  end
  object titL: TLabel
    Left = 64
    Top = 24
    Width = 49
    Height = 39
    Caption = 'titL'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -31
    Font.Name = 'Signika'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object timpL: TLabel
    Left = 65
    Top = 61
    Width = 48
    Height = 23
    Caption = 'timpL'
    Font.Charset = ANSI_CHARSET
    Font.Color = 774807
    Font.Height = -19
    Font.Name = 'Signika'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Image2: TImage
    Left = 33
    Top = 120
    Width = 350
    Height = 90
    Visible = False
  end
  object colSH: TShape
    Left = 33
    Top = 24
    Width = 25
    Height = 75
    Brush.Color = 14209483
    Pen.Style = psClear
  end
  object Label1: TLabel
    Left = 576
    Top = 120
    Width = 72
    Height = 31
    Caption = 'Label1'
    Font.Charset = ANSI_CHARSET
    Font.Color = 14999772
    Font.Height = -25
    Font.Name = 'Signika'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label2: TLabel
    Left = 576
    Top = 149
    Width = 46
    Height = 20
    Caption = 'Label2'
    Font.Charset = ANSI_CHARSET
    Font.Color = 774807
    Font.Height = -16
    Font.Name = 'Signika'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
end
