unit Cursuri;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFCurs = class(TForm)
    Image1: TImage;
    CloseB: TImage;
    titL: TLabel;
    timpL: TLabel;
    Image2: TImage;
    colSH: TShape;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CloseBClick(Sender: TObject);
  private
    procedure SetIGVisibility(x: byte; visible: boolean);
    procedure RefreshInfo;
    procedure RefreshIGBusiness(x: byte; s, st: string);
    procedure RepositionIGs;
  public
    { Public declarations }
  end;

type TIGStats=record
  LengthMin: word;
end;

type TImageGroup=record
  ContainerImg: TImage;
  Items: array of TImage;
  Stats: TIGStats;
end;

var
  FCurs: TFCurs;
  ig: array of TImageGroup;
  nig: byte;

implementation

{$R *.dfm}

uses DataTypes, FunctiiBaza, Main, VCLCustomMethods, XML.VerySimple, UIntList;

procedure TFCurs.SetIGVisibility(x: byte; visible: Boolean);
var i: byte;
begin
  if length(ig[x].Items)>0 then for i:=0 to length(ig[x].Items)-1 do ig[x].Items[i].Visible:=visible;
  ig[x].ContainerImg.Visible:=visible
end;

procedure TFCurs.CloseBClick(Sender: TObject);
begin
  FCurs.Close
end;

procedure TFCurs.FormCreate(Sender: TObject);
begin
  CloseB.OnMouseEnter:=FMain.ButtonIMGMouseEnter;
  CloseB.OnMouseLeave:=FMain.ButtonIMGMouseLeave;
  CloseB.OnMouseLeave(CloseB)
end;

procedure TFCurs.FormShow(Sender: TObject);
begin
  RefreshInfo
end;

procedure TFCurs.RepositionIGs;
var i, j: word;
begin
  if nig=1 then
    begin
      ig[0].ContainerImg.Left:=FCurs.Width div 2-Image2.Width div 2;
      if length(ig[0].Items)>0 then for j:=0 to length(ig[0].Items)-1 do
        begin ig[0].Items[j].Left:=ig[0].ContainerImg.Left+20; ig[0].Items[j].Width:=ig[0].ContainerImg.Width-40 end
    end
  else
    for i:=0 to nig-1 do
      begin
        ig[i].ContainerImg.Left:=Image2.Left+i*(Image2.Width+8);
        if length(ig[i].Items)>0 then for j:=0 to length(ig[i].Items)-1 do
          begin ig[i].Items[j].Left:=ig[i].ContainerImg.Left+20; ig[i].Items[j].Width:=ig[i].ContainerImg.Width-40 end
      end
end;

procedure TFCurs.RefreshInfo;
var i, oLen: word; totLengthMin, totItemi: longint; node: TXMLNode;
begin
  node:=Orar.Orar.Find('disciplina', 'nume', titL.Caption);
  colSH.Brush.Color:=HTMLToTColor(node.Attribute['col']);
  //
  nig:=node.FindNodes('item').Count;
  if nig>length(ig) then
    begin
      oLen:=length(ig); setlength(ig, nig);
      for i:=oLen to nig-1 do
        begin
          ig[i].ContainerImg:=TImage.Create(Self);
          with ig[i].ContainerImg do
            begin Parent:=FCurs; Top:=Image2.Top; Left:=Image2.Left+i*(Image2.Width+8); Width:=Image2.Width; Height:=250 end
        end
    end;
  for i:=0 to length(ig)-1 do SetIGVisibility(i, i<nig);
  //
  for i:=0 to nig-1 do
    RefreshIGBusiness(i, titL.Caption, node.FindNodes('item')[i].Attribute['tip']);
  //
  totLengthMin:=0; totItemi:=0;
  for i:=0 to nig-1 do begin inc(totLengthMin, ig[i].Stats.LengthMin); inc(totItemi, length(ig[i].Items)) end;
  timpL.Caption:=Format('%d:%.2d �n %s', [totLengthMin div 60, totLengthMin mod 60, plural('item', totItemi, true)]);
  //
  Image1.Width:=max(2, nig)*(Image2.Width+8)-8+2*(Image2.Left-Image1.Left);
  Image1.Height:=max(200, ig[0].ContainerImg.top+ig[0].ContainerImg.Height+32);
  ColorImagePanel(Image1, colForm, colTitleImgNormal);
  FCurs.Width:=Image1.Width+2*Image1.Left; FCurs.Height:=Image1.Height+2*Image1.Top;
  RepositionIGs;
  CloseB.Left:=FCurs.Width-CloseB.Width-23;
  FCurs.Left:=Screen.Width div 2-FCurs.Width div 2; FCurs.Top:=Screen.Height div 2-FCurs.Height div 2
end;

procedure TFCurs.RefreshIGBusiness(x: Byte; S, ST: string);
var i: word; pl: TIntList;
begin
  ig[x].Stats.LengthMin:=0;
  // Find items S of type ST
  pl:=TIntList.Create; pl.Sorted:=false; pl.Duplicates:=dupIgnore;
  for i:=0 to Orar.nIO-1 do
    if (Orar.IntervOrar[i].Disciplina=S) and (Orar.IntervOrar[i].Tip=ST) then
      pl.Add(i);
  for i:=0 to pl.Count-1 do
    begin
      inc(ig[x].Stats.LengthMin, nMinutes(Orar.IntervOrar[pl[i]].deLa, Orar.IntervOrar[pl[i]].panaLa));
    end;
  //
  ig[x].ContainerImg.Picture:=nil;
  with ig[x].ContainerImg.Canvas do
    begin
      //this is from ColorImagePanel()
      Brush.Style:=bsSolid; Brush.Color:=colForm; FillRect(Rect(0, 0, Image2.Width+2, ig[x].ContainerImg.Height+2));
      Pen.Style:=psSolid; Pen.Color:=colTitleImgNormal; Pen.Width:=16; Rectangle(0, 0, Image2.Width+2, ig[x].ContainerImg.Height+2);
      //
      Font:=Label1.Font; TextOut(20, 10, st);
      Font:=Label2.Font; TextOut(20, 35, Format('%d:%.2d �n %s', [ig[x].Stats.LengthMin div 60, ig[x].Stats.LengthMin mod 60, plural('item', length(ig[x].Items), true)]));
    end
end;

end.
