unit About;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFAbout = class(TForm)
    Image1: TImage;
    titL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    OkB: TImage;
    procedure FormCreate(Sender: TObject);
    procedure OkBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAbout: TFAbout;

implementation

{$R *.dfm}

uses Main, VCLCustomMethods;

procedure TFAbout.FormCreate(Sender: TObject);
begin
  OkB.Left:=FAbout.Width div 2-OkB.Width div 2;
  OkB.OnMouseEnter:=FMain.ButtonIMGMouseEnter;
  OkB.OnMouseLeave:=FMain.ButtonIMGMouseLeave;
  Okb.OnMouseLeave(OkB);
  ColorImagePanel(Image1, colForm, colTitleImgNormal)
end;

procedure TFAbout.OkBClick(Sender: TObject);
begin
  FAbout.Close
end;

end.
