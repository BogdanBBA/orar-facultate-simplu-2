object FCalendar: TFCalendar
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsNone
  Caption = 'FCalendar'
  ClientHeight = 399
  ClientWidth = 489
  Color = 3946803
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -19
  Font.Name = 'Signika'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  ScreenSnap = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 22
  object Image1: TImage
    Left = 8
    Top = 8
    Width = 473
    Height = 383
  end
  object titL: TLabel
    Left = 40
    Top = 32
    Width = 163
    Height = 27
    Caption = 'Alege o noua data'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -21
    Font.Name = 'Signika'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LoadB: TImage
    Left = 40
    Top = 278
    Width = 200
    Height = 75
    Cursor = crHandPoint
    Hint = 'Ok'
    OnClick = LoadBClick
  end
  object CancelB: TImage
    Left = 246
    Top = 278
    Width = 200
    Height = 75
    Cursor = crHandPoint
    Hint = 'Anuleaza'
    OnClick = CancelBClick
  end
  object subtitL: TLabel
    Left = 40
    Top = 65
    Width = 163
    Height = 19
    Caption = 'Alege din lista de mai jos:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -15
    Font.Name = 'Signika'
    Font.Style = []
    ParentFont = False
  end
  object cal: TMonthCalendar
    Left = 120
    Top = 105
    Width = 247
    Height = 143
    AutoSize = True
    CalColors.BackColor = clYellow
    CalColors.TitleBackColor = 33023
    Date = 41334.605053090280000000
    FirstDayOfWeek = dowMonday
    ShowToday = False
    TabOrder = 0
    WeekNumbers = True
    OnClick = calClick
  end
end
